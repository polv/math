import math
from decimal import Decimal


class Int:
    _first = ""
    _last = ""
    _length = None  # Int

    val: int = None  # actual int, if applicable

    def __init__(self, val: int = 0) -> None:
        if isinstance(val, Int):
            if val.val is None:
                self._first = val._first
                self._last = val._last
                self._length = val._length
                return
            self.val = val.val
        else:
            self.val = val

        try:
            float(self.val)
        except OverflowError:
            self._first = self.first
            self._last = self.last
            self._length = self.length

            self.val = None

    @property
    def first(self) -> str:
        if self._first:
            return self._first

        return str(self.val)[:8]

    @property
    def last(self) -> str:
        if self._last:
            return self._last

        return str(self.val)[-8:]

    @property
    def length(self):
        if self._length:
            return self._length

        return Int(len(str(self.val)))

    def exp3(self):
        return Int(3) ** self.val

    def tetrate3(self, n: int):
        first = Int(self)
        for _ in range(n - 1):
            first = first.exp3()

        return first

    def __repr__(self) -> str:
        if self.val is None:
            return f"{self.first}...{self.last} ({self.first[0]}.{self.first[1:]}e+{self.length})"

        return f"{self.val}"

    def __pow__(self, _other):
        base = Int(self)
        exp = Int(_other)

        if base.val and exp.val:
            try:
                float(base.val) ** exp.val
                return Int(base.val ** exp.val)
            except OverflowError:
                pass

        log = Decimal(exp.val) * Decimal(math.log10(base.val))
        fl = math.floor(float(log))

        out = Int()
        out._first = f"{(10 ** float(log - fl)):.7f}".replace(".", "")
        # out._last = tetmod_digits(int(base.last), 4)
        out._last = modpow_digits(int(base.last), exp.val)
        out._length = Int(fl)
        out.val = None

        return out


def modpow(base: int, exp: int, mod: int) -> int:
    r = 1
    base = base % mod
    while exp:
        if exp % 2:
            r = (r * base) % mod

        exp = exp // 2
        base = (base ** 2) % mod

    return r


def modpow_digits(base: int, exp: int, digits: int = 8) -> str:
    mod = 10 ** digits
    return str(modpow(base, exp, mod)).rjust(digits, "0")


def tetmod(base: int, k: int, mod: int) -> int:
    if k == 1:
        return base % mod
    elif k == 2:
        return modpow(base, base, mod)

    phi = euler_phi(mod)
    return modpow(base, phi + tetmod(base, k - 1, phi) % phi, mod)


def tetmod_digits(base: int, k: int, digits: int = 8) -> str:
    mod = 10 ** digits
    return str(tetmod(base, k, mod)).rjust(digits, "0")


def euler_phi(n: int) -> int:
    # Initialize result as n
    result = n

    # Consider all prime factors
    # of n and subtract their
    # multiples from result
    p = 2
    while p * p <= n:

        # Check if p is a
        # prime factor.
        if n % p == 0:

            # If yes, then
            # update n and result
            while n % p == 0:
                n = int(n / p)
            result -= int(result / p)
        p += 1

    # If n has a prime factor
    # greater than sqrt(n)
    # (There can be at-most
    # one such prime factor)
    if n > 1:
        result -= int(result / n)
    return result


if __name__ == "__main__":
    # After the third digits may be imprecise
    # => 12579723...00739387 (1.2579723e+3638334640024)
    print(Int(3).tetrate3(4))
