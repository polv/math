import os

k_cubed = dict()  # k^3 = v


class Int:
    MAX_LEN = 10

    def __init__(self, val: int) -> None:
        if isinstance(val, Int):
            self.val = val.val
        else:
            self.val = val

    def exp3(self):
        return Int(3).pow(self.val)

    def tetrate3(self, n: int):
        first = Int(self)
        for _ in range(n - 1):
            first = first.exp3()

        return first

    def __repr__(self) -> str:
        s = str(self.val)

        if len(s) > self.MAX_LEN:
            h = self.MAX_LEN // 2
            return f"{s[:h]}...{s[-h:]} ({s[0]}.{s[1:4]}e+{Int(len(s))})"

        return s

    def pow(self, _other):
        first = Int(self)
        other = Int(_other)

        if other.val > 3:
            # print("calculating base")
            base = k_cubed.get(first.val)
            if base is None:
                base = Int(first.val ** 3)
                with open(f"cache/k_cubed/{first}", "w") as f:
                    f.writelines((str(first.val), "\n", str(base.val), "\n"))
            # print(f"done: {base}")

            # print("calculating exp")
            exp = other.val // 3
            # print(f"done: {exp}")

            print(base, exp)
            out = base.pow(exp)

            print(out)

            return out
        elif other.val == 3:
            return Int(first.val ** 3)

        return first


for filename in os.listdir("cache/k_cubed"):
    with open(f"cache/k_cubed/{filename}", "r") as f:
        lines = f.readlines()
        if len(lines) != 2:
            raise ValueError(f"Invalid file: {f}")

        k, v = lines
        k_cubed[int(k)] = Int(int(v))


if __name__ == "__main__":
    print(Int(3).tetrate3(4))
