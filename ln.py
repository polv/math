from decimal import Decimal, getcontext
from typing import Union


def ln(k: Union[int, float, Decimal], precision: int = None) -> Decimal:
    ctx = getcontext()

    if precision:
        if ctx.prec <= precision:
            ctx.prec = precision + 1
    else:
        precision = ctx.prec - 1

    x = None
    is_shuffle = k > 1
    if is_shuffle:
        x = Decimal(1 - k) / k
    else:
        x = Decimal(1 - k)

    out = x
    i = 2
    target = 1 / 10 ** (precision - 1)

    while True:
        next = x ** i / i

        if abs(next) < target:
            break

        if is_shuffle:
            next = next * (1 if i % 2 else -1)

        out += next
        i += 1

    return out * (1 if is_shuffle else -1)


if __name__ == "__main__":
    # => 0.4771213472232960863248236375
    print(ln(3) / ln(10))
    # => -0.7402387880937958779452278460
    print(ln(0.477))
