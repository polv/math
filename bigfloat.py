import math


class F:
    sign: int = 1

    def __init__(self, m: float, n=None) -> None:
        if isinstance(m, F):
            self.m = m.m
            self.n = m.n
        else:
            if m is None:
                m = 0

            self.m = float(m)
            self.n = n

        if self.m < 0:
            self.sign = -1
            self.m = -self.m

        if self.m >= 10:
            log = math.log10(self.m)
            fl = math.floor(log)

            self.m = 10 ** (log - fl)

            if not self.n:
                self.n = F(fl)
            else:
                self.n.m += fl

        if not self.n:
            self.n = None

    def __str__(self) -> str:
        f_self = float(self)

        def simp_str(f: float) -> str:
            if f - math.floor(f) < 0.001:
                return f"{round(f)}"

            return f"{f:.3f}"

        if f_self < 1000:
            return simp_str(f_self)

        out = simp_str(self.m)

        if self.n:
            n_str = str(self.n)
            if "e" in n_str:
                n_str = f"({n_str})"

            out += f"e+{n_str}"

        return out

    def __float__(self) -> float:
        out = 0.0

        if self.n:
            out = float(self.n)

        if out == math.inf:
            return out

        return 10 ** out * self.m

    def __int__(self) -> int:
        return round(float(self))

    def __eq__(self, o: object) -> bool:
        if o is None:
            return self.n is None and self.m == 1

        f_self = float(self)

        if f_self == math.inf:
            return False

        return F(o) == f_self

    def __bool__(self) -> bool:
        return self.m != 0

    def __neg__(self):
        new_self = F(self)
        new_self.sign = -new_self.sign
        return new_self

    def __sub__(self, _other):
        return self + (-F(_other))

    def __add__(self, _other):
        other = F(_other)

        if not self.n and not other.n:
            return F(self.m + other.m)

        a1, a2 = self, other
        if float(a2) > float(a1):
            a1, a2 = a2, a1

        steps = F(a1.n) - a2.n
        if float(steps) >= 3:
            return a2

        a1 = F(a1)
        a1.m = a1.m + (a2.m / int(steps))

        return a1

    def __mul__(self, _other):
        other = F(_other)
        if not other.n:
            other.n = F(0)

        other.m = self.m * other.m

        if self.n:
            other.n = self.n + other.n

        if not other.n or not other.m:
            other.n = None

        return other

    def __pow__(self, _other):
        other = F(_other)

        f_self = float(self)
        f_other = float(other)

        if f_self != math.inf and f_other != math.inf:
            try:
                return F(f_self ** f_other)
            except OverflowError:
                pass

        out = F(0)
        out_tmp = other ** self.m

        out.m = out_tmp.m
        out.n = out_tmp.n + other * self.n

        return F(out)


if __name__ == "__main__":
    print(F(3) ** F(3) ** F(3) ** F(3))
