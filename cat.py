def notarize(val: str) -> str:
    val = val.strip()
    length = len(val)

    if length > 16:
        last = val[-8:]
        first = val[:8]

        s = ""
        if last:
            s = f"{first}...{last}"

        e = ""
        if length:
            m = ""
            if first:
                m = first[0]

                if len(first) > 1:
                    m += "." + first[1:]

            e = f"{m}e+{length}"

        if s:
            if e:
                return f"{s} ({e})"
            return s
        return e

    return val


if __name__ == "__main__":
    import sys

    print(notarize(sys.stdin.read()))
